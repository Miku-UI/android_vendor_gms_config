#
# Copyright (C) 2021 The Android Open Source Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Automated
$(call inherit-product, vendor/gms/main/main-vendor.mk)

ifneq ($(GMS_PIXEL_FEATURE),false)
$(call inherit-product, vendor/gms/extra/extra-vendor.mk)
endif

# DEX
DONT_UNCOMPRESS_PRIV_APPS_DEXS := true

# Overlay
PRODUCT_PACKAGES += \
    GlobalGmsConfig \
    GmsConfig \
    GmsConfigASI \
    GmsConfigGeotz \
    GmsConfigSysUI \
    GmsSettings \
    GmsSettingsProvider \
    GmsTelecomm \
    GmsTelephony \
    PixelSetupWizardConfig \
    PixelSimpleDeviceConfig \
    TurboConfig

# Properties
# ACSA
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.acsa=true

# Cient ID
ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.com.google.clientidbase=android-google
else
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

# OPA
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

# Setup Wizard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.day_night_mode_enabled=true \
    setupwizard.feature.lifecycle_refactoring=true \
    setupwizard.feature.notification_refactoring=true \
    setupwizard.feature.portal_notification=true \
    setupwizard.feature.show_digital_warranty=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=false \
    setupwizard.feature.show_support_link_in_deferred_setup=false \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.theme=glif_v4_light

# Version
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.gmsversion=14_frc1
